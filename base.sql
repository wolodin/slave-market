CREATE TABLE IF NOT EXISTS `slaves` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(255),
    `sex`  char(1) NOT NULL DEFAULT 'm',
    `age`  int,
    `weight` int,
    `color_skin` varchar(255),
    `catch` varchar(255),
    `description` mediumtext,
    `price` int NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    INDEX `weight_price_idx` (`weight`, `price`)
) ENGINE=INNODB ;

CREATE TABLE IF NOT EXISTS `categories` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(255),
    `parent_id` int unsigned NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    INDEX `parent_id_idx` (`parent_id`)
) ENGINE=INNODB ;

CREATE TABLE IF NOT EXISTS `categories_slaves_relationship` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    `slave_id` int unsigned NOT NULL,
    `category_id` int unsigned NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `slaves_FK_1` FOREIGN KEY (`slave_id`) REFERENCES `slaves` (`id`),
    CONSTRAINT `categories_FK_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
    INDEX `slave_id_idx` (`slave_id`),
    INDEX `category_id_idx` (`category_id`)
) ENGINE=INNODB ;


/* Получить минимальную, максимальную и среднюю стоимость всех рабов весом более 60 кг. При этом если AVG вынести в отдельный запрос то тогда запрос с MIN/MAX будет выполняться с индексом */
SELECT MIN(`price`), MAX(`price`),  AVG(`price`) FROM `slaves` WHERE `weight` > 60;

/* Категории, в которых больше 10 рабов. */
SELECT `c`.`name`, `c`.`id`, count(`cr`.`slave_id`)
    FROM `categories` AS `c`
    INNER JOIN `categories_slaves_relationship` AS `cr` ON `cr`.`category_id` = `c`.`id`
    GROUP BY `c`.`id`
    HAVING COUNT(`cr`.`slave_id`) > 10;

/* Категорию с наибольшей суммарной стоимостью рабов. */
SELECT `c`.`name` AS `name`, `c`.`id` AS `id`, SUM(`s`.`price`)
    FROM `categories` AS `c`
    INNER JOIN `categories_slaves_relationship` AS `cr` ON `cr`.`category_id` = `c`.`id`
    INNER JOIN `slaves` AS `s` ON `cr`.`slave_id` = `s`.`id`
    GROUP BY `c`.`id`
    ORDER BY SUM(`s`.`price`) DESC  LIMIT 1;

/* Категории, в которых мужчин больше, чем женщин. */
SELECT `c`.`name` AS `name`, `c`.`id` AS `id`,
       SUM(`s`.`sex` = 'm') AS SM, SUM(`s`.`sex` = 'f') AS SF
    FROM `categories` AS `c`
    INNER JOIN `categories_slaves_relationship` AS `cr` ON `cr`.`category_id` = `c`.`id`
    INNER JOIN `slaves` as `s` ON `cr`.`slave_id` = `s`.`id`
    GROUP BY `c`.`id`
    HAVING
    SUM(`s`.`sex` = 'm') > SUM(`s`.`sex` = 'f')

/* Количество рабов в категории "Для кухни" (включая все вложенные категории). */
SELECT COUNT(`cr`.`slave_id`)
    FROM `categories_slaves_relationship` AS `cr`
    INNER JOIN `categories` AS `c` ON `cr`.`category_id` = `c`.`id`
    WHERE `c`.`name` = 'Для кухни' OR `c`.`parent_id` IN (id подкатегорий через запятую);