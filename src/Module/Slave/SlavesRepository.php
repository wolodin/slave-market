<?php

namespace SlaveMarket\Module\Slave;

use SlaveMarket\Module\Slave\Slave;

/**
 * Репозиторий рабов
 *
 * @package SlaveMarket
 */
interface SlavesRepository
{
    /**
     * Возвращает раба по его id
     *
     * @param int $id
     * @return Slave
     */
    public function getById(int $id): Slave;
}