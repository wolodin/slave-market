<?php

namespace SlaveMarket\Module\Lease\LeaseContract;

use SlaveMarket\Module\Slave\Slave;
use SlaveMarket\Module\Master\Master;
use SlaveMarket\Module\Lease\LeaseHour;

/**
 * Договор аренды
 *
 * @package SlaveMarket\Lease
 */
class LeaseContract
{
    /** @var \SlaveMarket\Module\Master\Master Хозяин */
    public $master;

    /** @var \SlaveMarket\Module\Slave\Slave Раб */
    public $slave;

    /** @var float Стоимость */
    public $price = 0;

    /** @var LeaseHour[] Список арендованных часов */
    public $leasedHours = [];

    public function __construct(Master $master, Slave $slave, float $price, array $leasedHours)
    {
        $this->master      = $master;
        $this->slave       = $slave;
        $this->price       = $price;
        $this->leasedHours = $leasedHours;
    }
}
