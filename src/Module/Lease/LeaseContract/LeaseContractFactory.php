<?php

namespace SlaveMarket\Module\Lease\LeaseContract;

use DatePeriod;
use DateInterval;
use SlaveMarket\Module\Slave\Slave;
use SlaveMarket\Module\Master\Master;
use SlaveMarket\Module\Lease\LeaseHour;

class LeaseContractFactory
{
    private const MAX_TIME_WORK = 16;
    
    public function build(
        Slave $slave,
        Master $master,
        \DateTimeImmutable $timeFrom,
        \DateTimeImmutable $timeTo
    ): LeaseContract
    {
        $firstTime  = new LeaseHour($timeFrom->format('Y-m-d H'));
        $secondTime = new LeaseHour($timeTo->format('Y-m-d H'));
        
        $interval = $secondTime->getDateTime()->diff($firstTime->getDateTime());
        $price    = $this->priceCalculation($interval, $slave);
        
        $leaseHours = $this->formLeaseHours($firstTime, $secondTime);
        
        return new LeaseContract(
            $master,
            $slave,
            $price,
            $leaseHours
        );
    }
    
    /**
     * @param DateInterval                    $interval
     * @param \SlaveMarket\Module\Slave\Slave $slave
     *
     * @return float
     */
    private function priceCalculation(DateInterval $interval, Slave $slave): float
    {
        return $interval->h === 0 ? $slave->getPricePerHour() : (($interval->h + ($interval->d * self::MAX_TIME_WORK)) * $slave->getPricePerHour());
    }
    
    /**
     * @param LeaseHour $firstTime
     * @param LeaseHour $secondTime
     *
     * @return LeaseHour[]
     */
    private function formLeaseHours(LeaseHour $firstTime, LeaseHour $secondTime): array
    {
        $leaseHours = [];
        $interval   = new DateInterval('PT1H');
        $hours      = new DatePeriod($firstTime->getDateTime(), $interval, $secondTime->getDateTime());
        foreach ($hours as $hour) {
            $leaseHours[] = new LeaseHour($hour->format('Y-m-d H'));
        }
        
        return $leaseHours;
    }
}