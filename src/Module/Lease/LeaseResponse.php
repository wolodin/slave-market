<?php

namespace SlaveMarket\Module\Lease;

use SlaveMarket\Module\Lease\LeaseContract\LeaseContract;

/**
 * Результат операции аренды
 *
 * @package SlaveMarket\Lease
 */
class LeaseResponse
{
    /** @var \SlaveMarket\Module\Lease\LeaseContract\LeaseContract договор аренды */
    protected $leaseContract;

    /** @var string[] список ошибок */
    protected $errors = [];

    /**
     * Возвращает договор аренды, если аренда была успешной
     *
     * @return \SlaveMarket\Module\Lease\LeaseContract\LeaseContract
     */
    public function getLeaseContract(): ?LeaseContract
    {
        return $this->leaseContract;
    }

    /**
     * Указать договор аренды
     *
     * @param \SlaveMarket\Module\Lease\LeaseContract\LeaseContract $leaseContract
     */
    public function setLeaseContract(LeaseContract $leaseContract)
    {
        $this->leaseContract = $leaseContract;
    }

    /**
     * Сообщить об ошибке
     *
     * @param string $message
     */
    public function addError(string $message): void
    {
        $this->errors[] = $message;
    }

    /**
     * Возвращает все ошибки в процессе аренды
     *
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}