<?php

namespace SlaveMarket\Module\Master;

use SlaveMarket\Module\Master\Master;

/**
 * Репозиторий хозяев
 *
 * @package SlaveMarket
 */
interface MastersRepository
{
    /**
     * Возвращает хозяина по его id
     *
     * @param int $id
     * @return Master
     */
    public function getById(int $id) : Master;
}