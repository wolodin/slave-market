<?php

namespace SlaveMarket\Module;

use DateInterval;
use DateTimeImmutable;

class ChangeTime
{
    public function timeRoundToDown(string $dateTime): DateTimeImmutable
    {
        $dateTimeImmutable = new DateTimeImmutable($dateTime);
    
        return $dateTimeImmutable->setTime(
            $dateTimeImmutable->format('H'),
            0
        );
    }
    
    public function timeRoundToLarge(string $dateTime): DateTimeImmutable
    {
        $timeStart = new DateTimeImmutable($dateTime);
        $timeOffset = $timeStart->add(new DateInterval('PT59M'));
        
        return $timeOffset->setTime(
            $timeOffset->format('H'),
            0
        );
    }

}
