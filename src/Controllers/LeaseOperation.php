<?php

namespace SlaveMarket\Controllers;

use SlaveMarket\Module\ChangeTime;
use SlaveMarket\Module\Lease\LeaseHour;
use SlaveMarket\Module\Master\MastersRepository;
use SlaveMarket\Module\Slave\SlavesRepository;
use SlaveMarket\Module\Lease\LeaseRequest;
use SlaveMarket\Module\Lease\LeaseResponse;
use SlaveMarket\Module\Lease\LeaseContract\LeaseContractFactory;
use SlaveMarket\Module\Lease\LeaseContract\LeaseContractsRepository;

/**
 * Операция "Арендовать раба"
 *
 * @package SlaveMarket\Lease
 */
class LeaseOperation
{
    
    /**
     * @var \SlaveMarket\Module\Lease\LeaseContract\LeaseContractsRepository
     */
    private $contractsRepository;
    
    /**
     * @var \SlaveMarket\Module\Master\MastersRepository
     */
    private $mastersRepository;
    
    /**
     * @var SlavesRepository
     */
    private $slavesRepository;
    
    /**
     * @var ChangeTime
     */
    private $calculateTime;
    
    /**
     * @var LeaseContractFactory
     */
    private $leaseContractFactory;
    
    /**
     * LeaseOperation constructor.
     *
     * @param \SlaveMarket\Module\Lease\LeaseContract\LeaseContractsRepository $contractsRepo
     * @param \SlaveMarket\Module\Master\MastersRepository                     $mastersRepo
     * @param SlavesRepository                                                 $slavesRepo
     * @param ChangeTime                                                       $calculateTime
     * @param LeaseContractFactory                                             $leaseContractFactory
     */
    public function __construct(
        LeaseContractsRepository $contractsRepo,
        MastersRepository $mastersRepo,
        SlavesRepository $slavesRepo,
        ChangeTime $calculateTime,
        LeaseContractFactory $leaseContractFactory
    ) {
        $this->contractsRepository  = $contractsRepo;
        $this->mastersRepository    = $mastersRepo;
        $this->slavesRepository     = $slavesRepo;
        $this->calculateTime        = $calculateTime;
        $this->leaseContractFactory = $leaseContractFactory;
    }
    
    /**
     * Выполнить операцию
     *
     * @param LeaseRequest $request
     *
     * @return LeaseResponse
     */
    public function run(LeaseRequest $request): LeaseResponse
    {
        $response = new LeaseResponse();
        
        //Для округления
        $timeFrom = $this->calculateTime->timeRoundToDown($request->timeFrom);
        $timeTo   = $this->calculateTime->timeRoundToLarge($request->timeTo);
        
        $contracts = $this->contractsRepository->getForSlave($request->slaveId, $timeFrom->format('Y-m-d'), $timeTo->format('Y-m-d'));
        //Если контракта нет на это время то создаем
        if (empty($contracts)) {
            $this->makeContract($timeFrom, $timeTo, $request, $response);
        } else {
            $slave  = $this->slavesRepository->getById($request->slaveId);
            $master = $this->mastersRepository->getById($request->masterId);
            
            foreach ($contracts as $contract) {
                if (!$contract->master->isVIP() && $master->isVIP()) {
                    continue;
                }
                
                $leasedHours = $this->getLeaseHours($timeFrom, $timeTo, $contract->leasedHours);
                if ($leasedHours) {
                    $response->addError('Ошибка. Раб #' . $request->slaveId . ' "' . $slave->getName() . '" занят. Занятые часы: ' . $this->implodeHours($leasedHours));
                }
            }
            
            if (empty($response->getErrors())) {
                $this->makeContract($timeFrom, $timeTo, $request, $response);
            }
        }
        
        return $response;
    }
    
    /**
     * @param \DateTimeImmutable $timeFrom
     * @param \DateTimeImmutable $timeTo
     * @param LeaseHour[]        $leasedHours
     *
     * @return LeaseHour[]
     */
    private function getLeaseHours(
        \DateTimeImmutable $timeFrom,
        \DateTimeImmutable $timeTo,
        array $leasedHours
    ): array
    {
        $result = [];
        foreach ($leasedHours as $leasedHour) {
            $leasedHourDate = $leasedHour->getDateTime();
            if ($leasedHourDate >= $timeFrom && $leasedHourDate < $timeTo) {
                $result[] = $leasedHour;
            }
        }
        
        return $result;
    }
    
    /**
     * занятые часы
     *
     * @param LeaseHour[] $leaseHours
     *
     * @return string
     */
    private function implodeHours(array $leaseHours): string
    {
        $hours = [];
        foreach ($leaseHours as $leaseHour) {
            $hours[] = $leaseHour->getDateString();
        }
        
        return '"' . implode('", "', $hours) . '"';
    }
    
    private function makeContract(
        \DateTimeImmutable $timeFrom,
        \DateTimeImmutable $timeTo,
        LeaseRequest $request,
        LeaseResponse $response
    ): void
    {
        $slave    = $this->slavesRepository->getById($request->slaveId);
        $master   = $this->mastersRepository->getById($request->masterId);
        $contract = $this->leaseContractFactory->build($slave, $master, $timeFrom, $timeTo);
        
        $response->setLeaseContract($contract);
    }
    
}